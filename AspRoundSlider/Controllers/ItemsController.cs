﻿using AspRoundSlider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AspRoundSlider.Controllers
{
    public class ItemsController : Controller
    {
        private ItemsContext db = new ItemsContext();

        [HttpGet]
        public ActionResult Index()
        {
            var items = db.Items.ToList();
            return View(items);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var item = db.Items.Where(i => i.Id == id).FirstOrDefault();
            return View(item);
        }

        //public ActionResult GetItemPrice(int? id)
        //{
        //    if (id == null)
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    var itemPrice = db.Items.Where(i => i.Id == id).Select(p => p.Price);
        //    if (itemPrice == null)
        //        return HttpNotFound();
        //    return PartialView(itemPrice);
        //}
    }
}
