﻿(function ($, window, myApp) {
    $(document).ready(function () {
        $("#slider").roundSlider({
            radius: 90,
            width: 10,
            handleSize: "+10",
            sliderType: "range",
            value: myApp.item
        });
    });
})(jQuery, window, myApp);

//$(document).ready(function () {
//    $("#slider").roundSlider({
//        radius: 90,
//        width: 10,
//        handleSize: "+10",
//        sliderType: "range",
//        value: "0,100"
//    });


