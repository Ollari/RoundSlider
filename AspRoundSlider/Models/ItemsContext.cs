﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AspRoundSlider.Models
{
    public class ItemsContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public ItemsContext() : base("ItemEntity") { }
    }
}